CODE SAMPLES
Gabriel Strauss

1. for scotus-scraper project, see README.txt within scotus-scraper-master directory for project description and usage.
2. for headline-generation project, see headline_generation.ipynb for description and implementation of project. NOTE: All work in main file is my own; the attention_decoder module, however, was provided to us.