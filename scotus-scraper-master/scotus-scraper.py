#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 13 19:41:41 2017

@author: gabrielstrauss
"""

import requests
from bs4 import BeautifulSoup
import sys
import pickle
import case
import os

usage = "USAGE: ./case-list-scraper.py [start_year] [end_year]\n" +\
        "both args must be integer values such that 1760 " +\
        "<= start_year < end_year <= [current year]"




#thanks to StackOverflow contributors wim and MarcH (question 5574702)
def eprint(*args, **kwargs):
    
    print(*args, file=sys.stderr, **kwargs)
   
    
    
    
def verify_args(argv):
    
    if len(sys.argv) != 3:
        eprint("incorrect number of arguments\n", usage)
        sys.exit(1)
        
    start, end = sys.argv[1:]

    try:        
        int(start)
        int(end)
        
    except ValueError:        
        eprint("invalid arguments\n", usage)
        sys.exit(1)
    

    if not(1760 <= int(start) <= int(end) <= 2017):            
        eprint("invalid year range\n", usage)
        sys.exit(1)

    return start, end   
        
def parse_list(html):
    
   soup = BeautifulSoup(html, 'lxml')
   table = soup.find("table", id="srpcaselaw")
   #no cases from that year
   if table is None:
       return []
   
   tags = table.find_all('a')
   #return list of (title str,link str) for each case in table
   #tags[:-1] bc last element in table is "view more decisions"
   return [(tag['title'], tag['href']) for tag in tags[:-1]]


def scrape_all_cases(corpus):
    
    try:     
        os.stat(case.Case._Case__local_path)     
    except FileNotFoundError:      
        os.mkdir(case.Case._Case__local_path)
    
    for year in corpus:
        try:     
            os.stat(case.Case._Case__local_path + '/' + year)     
        except FileNotFoundError:      
            os.mkdir(case.Case._Case__local_path + '/' + year)
        for c in corpus[year]:
            stat = c.scrape()
            print(stat[1])

def main():
    
    start_year, end_year = verify_args(sys.argv)
        
    html_root = "http://caselaw.findlaw.com/court/us-supreme-court/years/"

    corpus = {}
    
    for year in range(int(start_year), int(end_year)+1):
        html = requests.get(html_root + str(year))
        case_list = parse_list(html.text)
        if year % 25 == 0:
            print(year)
        
        #no cases from that year
        if len(case_list) == 0:
            eprint("No cases from year " + str(year) + " in range")
            continue
        
        corpus[str(year)] = []

        for each in case_list:
            c = case.Case(each[0], each[1], year)
            corpus[str(year)].append(c)
            
    pickle.dump(corpus, open( "corpus_index.p", "wb" ))
    scrape_all_cases(corpus)
            
##############################################################################
 
main()
