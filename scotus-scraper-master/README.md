# scotus-scraper
Designed for those who want to do quantitative text analysis with SCOTUS cases but don't have access to databases like LexisNexis or WestLaw. More features and refined usage to come!
 
### Usage: 
```
python3 case_list_scraper.py [start_year] [stop_year]

'''start_year and stop_year must be integer values where:
1760 <= start_year < stop_year <= 2017'''
```

### Output
Cases are saved as .txt files named by docket number. Each year is saved in a separate directory. A pickled dictionary object containing metadata about the cases you've scraped is also saved.

The Supreme Court has heard cases every year from 1812 to the present. Before 1812, there are years in which no cases were heard. The program will write a message to stderr noting any years in the provided date range that have no cases.
