#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 16:38:31 2017

@author: gabrielstrauss
"""
import os.path
import requests
from bs4 import BeautifulSoup

class Case:
    #DO NOT CHANGE THIS ATTRIBUTE
    __local_path = 'SCOTUS'
    #tokens = COUNTER OBJECT
    
    def __init__(self, name, url, year):
        
        self.name = name
        self.url = url
        self.year = year     

        self.tagged = False
        self.counted = False
        
        self.opinions = []

    def get_path(self):
        
        u = self.url.split('/') #last two parts of url are case number
        #path = localpath/year/dockt-number (i.e. 'corpus/1967/18-27')
        return Case.__local_path + '/' + str(self.year) + '/' \
        +  u[-2] + '-' + (u[-1][0 : (len(u[-1]) - 5)]) + '.txt'
        
    def is_scraped(self):
        
        return os.path.isfile(self.get_path())
            
    def scrape(self):
        
        if self.is_scraped():
            
            return (0, ('already scraped' + self.name + "\n" \
                    + self.url + "\n" ))
        
        html = requests.get(self.url)
        
        if str(html) != '<Response [200]>':
            
            return (1, "Bad URL for " + self.name + "\n" \
                    + self.url + " returned " + str(html))
            
        soup = BeautifulSoup(html.text, 'lxml')
        body = soup.find(class_="caselawcontent searchable-content")
        
        if body is None:
            
            return (1, "Not scrapable: " + self.url \
                    + "\nDoes not appear to contain case text.")
            
        for h in ['h2', 'h3']:
            
            for tag in body.find_all(h):
                tag.decompose()
                
        with open(self.get_path(), "w", encoding='utf-8') as f:
            
            f.write(str(body.text))
            
        return (0, 'scraped' + self.name)

    def tag_sections():

        #some cases (i.e. "writ of certiorari dismissed") have no opinion at all
        #tag sections should perhaps happen BEFORE parsing beautiful soup
        #"Delivered the opinion of the court" often gets its own html <p> tag.
        #FEATURES INDICATING SENTENCE STATES WHO DELIVERS OPINION, DISSENT
        #1. Begins with "<Justice Name> J.", "Mr. [Chief] Justice", or simply "Opinion of the Court"
        #2. Includes "delivered the opinion of the court" or "dissenting"
        #3. Is only sentence within that <p> tag. If checking after BS parsing, can also just check
        #to see if there is a newline character before, since it is the beginning of a new section.
        #This will help eliminate instances in which justices are citing other opinions in the body of a paragraph.
        #1. parse entities delivering opinions, dissents.

        return NotImplementedError
